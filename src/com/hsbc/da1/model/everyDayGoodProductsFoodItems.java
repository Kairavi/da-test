package com.hsbc.da1.model;

public class everyDayGoodProductsFoodItems extends everyDayGoodProducts {

	private String dateOfManufacture;
	
	private String dateOfExpiry;
	
	private boolean isVegetarian;
	
	
	public everyDayGoodProductsFoodItems(long itemCode , String itemName , double unitPrice ,int itemQuantity, String dateOfManufacture , String dateOfExpiry,boolean isVegetarian)
	{
		 super(itemCode, itemName , unitPrice, itemQuantity);
		 
		 this.dateOfManufacture = dateOfManufacture;
		 
		 this.dateOfExpiry = dateOfExpiry;
		 
		 this.isVegetarian = isVegetarian;
		
	}


	public String getDateOfManufacture() {
		return dateOfManufacture;
	}


	public void setDateOfManufacture(String dateOfManufacture) {
		this.dateOfManufacture = dateOfManufacture;
	}


	public String getDateOfExpiry() {
		return dateOfExpiry;
	}


	public void setDateOfExpiry(String dateOfExpiry) {
		this.dateOfExpiry = dateOfExpiry;
	}


	public boolean isVegetarian() {
		return isVegetarian;
	}


	public void setVegetarian(boolean isVegetarian) {
		this.isVegetarian = isVegetarian;
	}


	

	
	
}
