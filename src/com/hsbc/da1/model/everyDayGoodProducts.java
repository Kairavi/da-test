package com.hsbc.da1.model;

public class everyDayGoodProducts {
	
	 private long itemCode;
	 
	 private String itemName;
	 
	 private double itemPrice;
	 
	 private int itemQuantity;
	 
	 private static long counter = 1000;
	 
	 everyDayGoodProductsElectronics electronics;
	 
	 everyDayGoodProductsFoodItems foodItem;
	 
	 everyDayGoodProductsApparel apparel;
	 
	 public everyDayGoodProducts(long itemCode , String itemName, double itemPrice, int itemQuantity)
	 {
		 this.itemCode = +counter;
		 
		 this.itemName = itemName;
		 
		 this.itemPrice = itemPrice;
		 
		 this.itemQuantity = itemQuantity;
		
		 
	 }

	 public int getItemQuantity() {
		return itemQuantity;
	}
	 
	 

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	
	
	public long getItemCode() {
			return itemCode;
		}

	
		public void setItemCode(long itemCode) {
			this.itemCode = itemCode;
		}

		
		public String getItemName() {
			return itemName;
		}

		
		public double getItemPrice() {
			return itemPrice;
		}

	 
	
	
}
