package com.hsbc.da1.model;

public class everyDayGoodProductsApparel extends  everyDayGoodProducts
{
   
	private int itemSize;
	
	private String itemMaterial;
	
	
	public everyDayGoodProductsApparel(long itemCode , String itemName, double itemPrice, int itemQuantity, int itemSize, String itemMaterial)
	
	{
		
	super(itemCode,itemName,itemPrice, itemQuantity);
	this.itemSize = itemSize;
	this.itemMaterial = itemMaterial;
	}


	public int getItemSize() {
		return itemSize;
	}


	public void setItemSize(int itemSize) {
		this.itemSize = itemSize;
	}


	public String getItemMaterial() {
		return itemMaterial;
	}


	public void setItemMaterial(String itemMaterial) {
		this.itemMaterial = itemMaterial;
	}
	
	
	
	
}

