package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.itemNotFound;
import com.hsbc.da1.model.everyDayGoodProducts;

public class everyDayGoodProductsFoodItemsDAOImpl implements everyDayGoodProductsdao {

	private List<everyDayGoodProducts> foodItemsList = new ArrayList<>();

	@Override
	public everyDayGoodProducts saveItems(everyDayGoodProducts item) {
		this.foodItemsList.add(item);
		return item;
	}

	@Override
	public everyDayGoodProducts updateItems(long itemCode, everyDayGoodProducts item) {
		for(everyDayGoodProducts e : foodItemsList) {
			if(e.getItemCode()== itemCode) {
				e = item;
			}
		}
		return item;
	}

	@Override
	public void deleteItem(long itemCode) {
		for(everyDayGoodProducts e : foodItemsList) {
			if(e.getItemCode()== itemCode) {
				this.foodItemsList.remove(e);
			}
		}

	}

	@Override
	public Collection<everyDayGoodProducts> fetchAllItems() {
		// TODO Auto-generated method stub
		return foodItemsList;
	}

	@Override
	public everyDayGoodProducts fetchItemByItemCode(long itemCode)throws itemNotFound {
		{
			for(everyDayGoodProducts e : foodItemsList) {
				if(e.getItemCode()== itemCode) {
					return e;
				}
			}
			throw new itemNotFound("item not found");
			return null;
		}
	}
	
	
	
	
}
