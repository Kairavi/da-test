package com.hsbc.da1.dao;

import java.util.Collection;

import com.hsbc.da1.model.everyDayGoodProducts;
import com.hsbc.da1.service.itemNotFound;


  interface everyDayGoodProductsdao {
	
	everyDayGoodProducts saveItems(everyDayGoodProducts item);
	
	everyDayGoodProducts updateItems(long itemCode, everyDayGoodProducts item)throws itemNotFound;
	
	void deleteItem(long itemCode)throws itemNotFound;
	
	Collection<everyDayGoodProducts> fetchAllItems();
	
	everyDayGoodProducts fetchItemByItemCode(long itemCode)throws itemNotFound;
	
	
	
	

}
