package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hsbc.da1.model.everyDayGoodProducts;
import com.hsbc.da1.service.itemNotFound;
import com.hsbc.da1.exception.itemNotFound;


public class everyDayGoodProductsElectronicsDAOImpl implements everyDayGoodProductsdao {

	private List<everyDayGoodProducts> electronicItemsList = new ArrayList<>();

	@Override
	public everyDayGoodProducts saveItems(everyDayGoodProducts item) {
		this.electronicItemsList.add(item);
		return item;
	}

	@Override
	public everyDayGoodProducts updateItems(long itemCode, everyDayGoodProducts item) {
		for(everyDayGoodProducts e : electronicItemsList) {
			if(e.getItemCode()== itemCode) {
				e = item;
			}
		}
		return item;
	}

	@Override
	public void deleteItem(long itemCode) {
		for(everyDayGoodProducts e : electronicItemsList) {
			if(e.getItemCode()== itemCode) {
				this.electronicItemsList.remove(e);
			}
		}

	}

	@Override
	public Collection<everyDayGoodProducts> fetchAllItems() {

		return electronicItemsList;
	}

	@Override
	public everyDayGoodProducts fetchItemByItemCode(long itemCode)throws itemNotFound {
		{
			for(everyDayGoodProducts e : electronicItemsList) {
				if(e.getItemCode()== itemCode) {
					return e;
				}
			}
			throw new itemNotFound("item not found");
			return null;
		}
	}
}

