package com.hsbc.da1.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.hsbc.da1.exception.itemNotFound;
//import com.hsbc.da1.exception.EmployeeNotFoundException;
//import com.hsbc.da1.model.Employee;
import com.hsbc.da1.model.everyDayGoodProducts;

public class everyDayGoodProductsApparelDAOImpl implements everyDayGoodProductsdao{
	
	private List<everyDayGoodProducts> apparelItemsList = new ArrayList<>();

	@Override
	public everyDayGoodProducts saveItems(everyDayGoodProducts item) {
		this.apparelItemsList.add(item);
		return item;
	}

	@Override
	public everyDayGoodProducts updateItems(long itemCode, everyDayGoodProducts item) {
		for(everyDayGoodProducts e : apparelItemsList) {
			if(e.getItemCode()== itemCode) {
				e = item;
			}
		}
		return item;
	}

	@Override
	public void deleteItem(long itemCode) {
		for(everyDayGoodProducts e : apparelItemsList) {
			if(e.getItemCode()== itemCode) {
				this.apparelItemsList.remove(e);
			}
		}

		
	}

	@Override
	public Collection<everyDayGoodProducts> fetchAllItems() {
		// TODO Auto-generated method stub
		return apparelItemsList;
		
	}

	@Override
	public everyDayGoodProducts fetchItemByItemCode(long itemCode) throws itemNotFound
	{
		for(everyDayGoodProducts e : apparelItemsList) {
			if(e.getItemCode()== itemCode) {
				return e;
			}
		}
		throw new itemNotFound("item not found");
		return null;
	}

	
	
	
}
