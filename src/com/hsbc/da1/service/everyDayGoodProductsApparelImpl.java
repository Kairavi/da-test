package com.hsbc.da1.service;

import java.util.Collection;

import com.hsbc.da1.dao.EmployeeManagementSystemDao;
import com.hsbc.da1.dao.everyDayGoodProductsApparelDAOImpl;
import com.hsbc.da1.model.everyDayGoodProducts;
import com.hsbc.da1.model.everyDayGoodProductsApparel;

public class everyDayGoodProductsApparelImpl implements everyDayGoodProductsService {

	
	private everyDayGoodProductsApparelDAOImpl dao;
	
	public everyDayGoodProductsApparelImpl(everyDayGoodProductsApparelDAOImpl dao) {
		this.dao = dao;
	}
	@Override
	public everyDayGoodProducts createItems(everyDayGoodProducts item) {
		// TODO Auto-generated method stub
	
		everyDayGoodProductsApparel apparelItem = new everyDayGoodProductsApparel(long itemCode , String itemName, double itemPrice, int itemQuantity, int itemSize, String itemMat)
				
				return this.dao.saveItems(apparelItem);
	
	}
	

	@Override
	public everyDayGoodProducts updateItems(long itemCode, everyDayGoodProducts item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteItem(long itemCode) {
		this.dao.deleteItem(itemCode);
		
	}

	@Override
	public Collection<everyDayGoodProducts> fetchAllItems() {
		return this.dao.fetchAllItems();
	}

	@Override
	public everyDayGoodProducts fetchEmployeeByItemCode(long itemCode) {
		return this.dao.fetchEmployeeByItemCode(itemCode);
	}

}
