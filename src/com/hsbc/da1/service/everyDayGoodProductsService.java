package com.hsbc.da1.service;

import java.util.Collection;

//import com.hsbc.da1.exception.EmployeeNotFoundException;
//import com.hsbc.da1.exception.NotEligibleForLeaveException;
import com.hsbc.da1.model.everyDayGoodProducts;

public interface everyDayGoodProductsService {
		
    everyDayGoodProducts createItems(everyDayGoodProducts item);
	
	everyDayGoodProducts updateItems(long itemCode, everyDayGoodProducts item)throws itemNotFound;
	
	void deleteItem(long itemCode)throws itemNotFound;
	
	Collection<everyDayGoodProducts> fetchAllItems();
	
	everyDayGoodProducts fetchEmployeeByItemCode(long itemCode) throws itemNotFound;

	
	
}
