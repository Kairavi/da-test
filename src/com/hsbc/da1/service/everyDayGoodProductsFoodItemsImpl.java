package com.hsbc.da1.service;

import java.util.Collection;

import com.hsbc.da1.dao.everyDayGoodProductsElectronicsDAOImpl;
import com.hsbc.da1.dao.everyDayGoodProductsFoodItemsDAOImpl;
import com.hsbc.da1.model.everyDayGoodProducts;
import com.hsbc.da1.model.everyDayGoodProductsFoodItems;

public class everyDayGoodProductsFoodItemsImpl implements everyDayGoodProductsService {

	
private everyDayGoodProductsFoodItemsDAOImpl dao;
	
	public void everyDayGoodProductsFoodItemsDAOImpl(everyDayGoodProductsFoodItemsDAOImpl dao) {
		this.dao = dao;
	}
	
	
	@Override
	public everyDayGoodProducts createItems(everyDayGoodProducts item) {
      everyDayGoodProductsFoodItems foodItem = new  everyDayGoodProductsFoodItems(long itemCode , String itemName, double itemPrice, int itemQuantity, boolean isVegetarian )
				
				return this.dao.saveItems(foodItem);

	}

	@Override
	public everyDayGoodProducts updateItems(long itemCode, everyDayGoodProducts item) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteItem(long itemCode) {
	     this.dao.deleteItem(itemCode);
		
	}

	@Override
	public Collection<everyDayGoodProducts> fetchAllItems() {
		return this.dao.fetchAllItems();
	}

	@Override
	public everyDayGoodProducts fetchEmployeeByItemCode(long itemCode) {
		return this.dao.fetchEmployeeByItemCode(itemCode);
	}

}
